/*
  option.h

  handle the conversion to GMT linked-list arguments

  Copyright (c) J.J. Green 2016
*/

#ifndef OPTION_H
#define OPTION_H

#include <ruby.h>
#include <gmt.h>

typedef struct GMT_OPTION gmt_option_t;

gmt_option_t* ruby_array_to_gmt_options(VALUE, void*);
gmt_option_t* append_inputs_to_gmt_options(VALUE, gmt_option_t*, void*);

#endif
