# more interestion as version-conditional test than in itself

describe 'grdtrend' do

  if GMT.version_at_least? 6, 0 then

    context 'GMT 6.0+' do

      it 'creates a file' do
        with_temporary_file('trend.grd') do |trend|
          GMT.session do |gmt|
            gmt.grdtrend(fixture('grid.grd'), N: '3+r', T: trend)
          end
          expect(File.exist? trend).to eq true
        end
      end
    end

  else

    context 'GMT prior to 6.0' do

      it 'raises' do
        with_temporary_file('trend.grd') do |trend|
          GMT.session do |gmt|
            expect {
              gmt.grdtrendo(fixture('grid.grd'), N: '3+r', T: trend)
            }.to raise_error(NoMethodError)
          end
          expect(File.exist? trend).to eq false
        end
      end
    end

  end
end
