describe 'The pstext module' do
  let(:gmt) { GMT.new }
  let(:options) do
    {
      R: '0/3/0/2',
      J: 'x1i',
      F: '+f12p,Helvetica-Bold+jCM+a0',
      B: 'a0.5f0.1'
    }
  end

  it 'creates valid PostScript' do
    with_temporary_file('pstext.ps') do |ps|
      gmt.pstext(fixture('text.txt'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
