describe 'The psscale module' do
  let(:gmt) { GMT.new }
  let(:options) do
    {
      D: 'jCT+w4i/1c+o0/2c+h',
      C: 'polar',
      B: 'af',
      J: 'X3i',
      R: '0/3/0/2',
    }
  end

  it 'creates valid PostScript' do
    with_temporary_file('psscale.ps') do |ps|
      gmt.psscale(:> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
