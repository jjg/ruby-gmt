describe 'The pslegend module' do
  let(:gmt) { GMT.new }
  let(:options) do
    {
      R: '-10/10/-10/10',
      J: 'M6i',
      F: '+gazure1',
      D: 'x0.5i/0.5i+w5i/3.3i+jBL+l1.2',
      C: '0.1i/0.1i',
      B: '5f1'
    }
  end

  it 'creates valid PostScript' do
    with_temporary_file('pslegend.ps') do |ps|
      gmt.pslegend(fixture('legend.txt'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
