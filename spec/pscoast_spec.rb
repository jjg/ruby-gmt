describe 'The pscoast module' do
  let(:gmt) { GMT.new }
  let(:options) do
    { R: '-30/-10/60/65', J: 'm1c', B: '5', G: '100' }
  end

  it 'creates valid PostScript' do
    with_temporary_file('pscoast.ps') do |ps|
      gmt.pscoast(:> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
