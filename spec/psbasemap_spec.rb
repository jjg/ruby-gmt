describe 'The psbasemap module' do
  let(:gmt) { GMT.new }
  let(:options) do
    { R: 'd', J: 'N0/8i', B: ['afg', '+tRobinson'] }
  end

  it 'creates valid PostScript' do
    with_temporary_file('psbasemap.ps') do |ps|
      gmt.psbasemap(:> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
