describe 'Multiple layers' do
  let(:gmt) { GMT.new }
  let(:options) do
    { R: '0/3/0/2', J: 'x1i' }
  end

  context 'without a middle layer' do

    it 'creates valid PostScript' do
      with_temporary_file('psxy.ps') do |ps|
        options.merge!(file: ps)
        gmt.psbasemap(position: :first, B: 1, **options)
        gmt.psxy(
          fixture('dots.txt'),
          position: :last,
          S: 'c0.25c',
          G: 'red',
          **options
        )
        expect(ps).to be_valid_postscript
      end
    end
  end

  context 'with a middle layer' do

    it 'creates valid PostScript' do
      with_temporary_file('psxy.ps') do |ps|
        options.merge!(file: ps)
        gmt.psbasemap(position: :first, B: 1, **options)
        gmt.pstext(
          fixture('text.txt'),
          position: :middle,
          F: '+f12p,Helvetica-Bold+jCM+a0',
          **options
        )
        gmt.psxy(
          fixture('dots.txt'),
          position: :last,
          S: 'c0.25c',
          G: 'red',
          **options
        )
        expect(ps).to be_valid_postscript
      end
    end
  end

  context 'with String positions' do

    it 'creates valid PostScript' do
      with_temporary_file('psxy.ps') do |ps|
        options.merge!(file: ps)
        gmt.psbasemap(position: 'first', B: 1, **options)
        gmt.psxy(
          fixture('dots.txt'),
          position: 'last',
          S: 'c0.25c',
          G: 'red',
          **options
        )
        expect(ps).to be_valid_postscript
      end
    end
  end

end
