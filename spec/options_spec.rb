describe 'Option handling' do
  let(:gmt) { GMT.new }

  context 'Floating point arguments' do
    let(:options) do
      { J: 'X5i', B: 'a0.5f0.1', C: 0.1 }
    end

    it 'creates valid PostScript' do
      with_temporary_file('grdcontour.ps') do |ps|
        gmt.grdcontour(fixture('grid.grd'), :> => ps, **options)
        expect(ps).to be_valid_postscript
      end
    end
  end

  context 'Integer arguments' do
    let(:options) do
      { R: '-30/-10/60/65', J: 'm1c', B: 5, G: 100 }
    end

    it 'creates valid PostScript' do
      with_temporary_file('pscoast.ps') do |ps|
        gmt.pscoast(:> => ps, **options)
        expect(ps).to be_valid_postscript
      end
    end
  end
end
