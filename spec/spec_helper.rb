require_relative '../lib/gmt.rb'

require 'fileutils'

Dir[File.dirname(__FILE__) + '/support/*/*.rb'].each do |path|
  require path
end

def with_a_silent(stream)
  old_stream = stream.dup
  stream.reopen('/dev/null')
  stream.sync = true
  yield
ensure
  stream.reopen(old_stream)
end

def with_temporary_file(name)
  path = File.join('/tmp', name)
  begin
    yield path
  ensure
    FileUtils.rm_f path
  end
end

def fixture(file)
  File.join(File.dirname(__FILE__), 'fixtures', file)
end
