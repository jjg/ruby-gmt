# The list of GMT modules varies depeneding on GMT version compiled,
# we attempt to track the list in ext/gmt.c with version conditionals,
# we duplicate that logic to get a list of modules for the current
# version here.

gmt_modules = [
  :blockmean,
  :blockmedian,
  :blockmode,
  :filter1d,
  :grdfilter,
  :gmtlogo,
  :grdcontour,
  :grdimage,
  :grdvector,
  :grdview,
  :psbasemap,
  :psclip,
  :pscoast,
  :pscontour,
  :pshistogram,
  :psimage,
  :pslegend,
  :psmask,
  :psrose,
  :psscale,
  :pstext,
  :pswiggle,
  :psxy,
  :psxyz,
  :greenspline,
  :nearneighbor,
  :sphinterpolate,
  :surface,
  :triangulate,
  :gmtsimplify,
  :grdsample,
  :grdtrack,
  :sample1d,
  :grdproject,
  :mapproject,
  :project,
  :gmtdefaults,
  :gmtget,
  :gmtinfo,
  :gmtset,
  :grdinfo,
  :gmtmath,
  :makecpt,
  :spectrum1d,
  :sph2grd,
  :sphdistance,
  :sphtriangulate,
  :gmtconnect,
  :gmtconvert,
  :gmtselect,
  :gmtspatial,
  :gmtvector,
  :grd2xyz,
  :grdblend,
  :grdconvert,
  :grdcut,
  :grdpaste,
  :splitxyz,
  :xyz2grd,
  :fitcircle,
  :gmtregress,
  :trend1d,
  :trend2d,
  :grd2cpt,
  :grdclip,
  :grdedit,
  :grdfft,
  :grdgradient,
  :grdhisteq,
  :grdlandmask,
  :grdmask,
  :grdmath,
  :grdvolume,
  :gmt2kml,
  :kml2gmt,
  :psconvert
]

if GMT.version_at_least? 5, 3 then
  gmt_modules << :pssolar
end

if GMT.version_at_least? 5, 4 then
  gmt_modules << :psternary
end

if GMT.version_at_least? 6, 0 then
  [
    :psevents,
    :dimfilter,
    :grd2kml,
    :grdtrend,
    :grdfill,
  ].each { |m| gmt_modules << m }
else
  gmt_modules << :grd2rgb
end

if GMT.version_at_least? 6, 1 then
  [
    :grdinterpolate,
    :grdmix,
    :grdgdal
  ].each { |m| gmt_modules << m }
end

if GMT.version_at_least? 6, 2 then
  [
    :gmtbinstats
  ].each { |m| gmt_modules << m }
end

if GMT.version_at_least? 6, 3 then
  [
    :grdselect
  ].each { |m| gmt_modules << m }
end

describe 'common behaviour' do

  shared_examples 'a GMT module' do
    let(:session) { GMT.new }
    let(:output_file) { '/tmp/documentation.txt' }

    after { session.free }

    describe 'short message on syntax on :^' do
      let(:opt) do
        {:^ => nil, :> => output_file}
      end

      it 'raises an Errno::ENOENT' do
        expect {
          with_a_silent(STDERR) do
            session.send(gmt_module, **opt)
          end
        }.to raise_error(Errno::ENOENT, /No such file or directory/)
      end
    end

    describe 'long message on usage on :+' do
      let(:opt) do
        {:+ => nil}
      end

      it 'raises a Errno::ENOENT' do
        expect {
          with_a_silent(STDERR) do
            session.send(gmt_module, **opt)
          end
        }.to raise_error(Errno::ENOENT, /No such file or directory/)
      end
    end

    describe 'extensive message on usage on :?' do
      let(:opt) do
        {:'?' => nil}
      end

      it 'raises a Errno::ENOENT' do
        expect {
          with_a_silent(STDERR) do
            session.send(gmt_module, **opt)
          end
        }.to raise_error(Errno::ENOENT, /No such file or directory/)
      end
    end
  end

  gmt_modules.each do |gmt_module|

    context "for the #{gmt_module} module" do

      it_behaves_like 'a GMT module' do
        let(:gmt_module) { gmt_module }
      end
    end
  end
end
