describe 'The xyz2grd module' do
  let(:gmt) { GMT.new }
  let(:options) do
    { R: '0/3/0/2', I: '0.5/0.5' }
  end

  it 'creates a valid NetCDF file' do
    with_temporary_file('grid.grd') do |grd|
      gmt.xyz2grd(fixture('grid.txt'), G: grd, **options)
      expect(grd).to be_valid_netcdf
    end
  end
end
