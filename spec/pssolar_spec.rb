describe 'The pssolar module' do
  let(:gmt) { GMT.new }
  let(:options) do
    {
      R: 'd',
      J: 'Q0/14c',
      W: '1p',
      T: 'dc'
    }
  end

  it 'creates valid PostScript' do
    skip unless GMT.version_at_least?(5, 3)
    with_temporary_file('pssolar.ps') do |ps|
      gmt.pssolar(:> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
