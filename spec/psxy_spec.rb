describe 'The psxy module' do
  let(:gmt) { GMT.new }
  let(:options) do
    {
      R: '0/3/0/2',
      J: 'x1i',
      S: 'c0.25c',
      G: 'red',
      B: 'a0.5f0.1',
    }
  end

  it 'creates valid PostScript' do
    with_temporary_file('psxy.ps') do |ps|
      gmt.psxy(fixture('dots.txt'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end

  it 'accepts multiple input files' do
    with_temporary_file('psxy.ps') do |ps|
      gmt.psxy(
        fixture('dots.txt'),
        fixture('dots.txt'),
        :> => ps, **options
      )
      expect(ps).to be_valid_postscript
    end
  end

  context 'when the input file does not exist' do
    let(:no_such_file) { fixture('no-such-file.xy') }
    before do
      if File.exist? no_such_file then
        skip 'non-existant file actually exists'
      end
    end

    it 'raises Errno::ENOENT' do
      with_temporary_file('psxy.ps') do |ps|
        expect {
          with_a_silent(STDERR) do
            gmt.psxy(no_such_file, :> => ps, **options)
          end
        }.to raise_error(Errno::ENOENT, /No such file or directory/)
      end
    end
  end
end
