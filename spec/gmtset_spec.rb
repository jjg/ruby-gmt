require 'tmpdir'

describe 'The gmtset module' do

  # regression - in 0.1 this fails with
  # TypeError: no implicit conversion of Symbol into String
  # ./lib/gmt.rb:177:in `delete'
  # ./lib/gmt.rb:177:in `coerce_append_option!'

  it 'does not need a options hash' do
    Dir.mktmpdir do |dir|
      Dir.chdir(dir) do
        GMT.session do |gmt|
          gmt.gmtset('FONT_ANNOT_PRIMARY', '10p,Courier-Oblique')
        end
      end
    end
  end
end
