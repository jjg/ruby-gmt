describe 'The makecpt module' do
  let(:gmt) { GMT.new }
  let(:range) { '-100/100/25' }

  it 'creates a file' do
    with_temporary_file('makecpt.cpt') do |cpt|
      gmt.makecpt(C: 'polar', T: range, :> => cpt)
      expect(cpt).to be_valid_cpt
    end
  end

  it 'accepts array arguments' do
    with_temporary_file('makecpt.cpt') do |cpt|
      gmt.makecpt(C: ['polar'], T: range, :> => cpt)
      expect(cpt).to be_valid_cpt
    end
  end

  it 'accepts nil arguments' do
    with_temporary_file('makecpt.cpt') do |cpt|
      gmt.makecpt(C: 'polar', T: range, I: nil, :> => cpt)
      expect(cpt).to be_valid_cpt
    end
  end

  it 'accepts symbol arguments' do
    with_temporary_file('makecpt.cpt') do |cpt|
      gmt.makecpt(:I, C: 'polar', T: range, :> => cpt)
      expect(cpt).to be_valid_cpt
    end
  end

  context 'if an unknown master palette is given' do

    it 'raises Errno::ENOENTr' do
      with_temporary_file('makecpt.cpt') do |cpt|
        expect {
          with_a_silent(STDERR) do
            gmt.makecpt(C: 'nosuchpalette', T: range, :> => cpt)
          end
        }.to raise_error(Errno::ENOENT)
      end
    end
  end
end
