describe 'The psimage module' do
  let(:gmt) { GMT.new }
  let(:options) do
    { D: 'x0/0+w1i', F: '+pthin,blue' }
  end

  # Note, psimage reading of raster files requires that GMT was
  # compiled with a version of GDAL which can read Sun raster
  # files -- I find this is not the case with the Ubuntu package
  # libgdal-dev 3.0.4+dfsg-1build3, so skip this for now ...

  it 'creates valid PostScript' do
    skip 'GDAL 3.0.4 issue on Ubuntu'
    with_temporary_file('psimage.ps') do |ps|
      gmt.psimage(fixture('duck.ras'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
